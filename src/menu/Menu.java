package menu;

public class Menu {
	
	public static void printMenuSystem() {
		System.out.println("================================================================");
		System.out.println("============================ MENU ==============================");
		System.out.println("= 1. CUSTOMER.                                                 =");
		System.out.println("= 2. BOOK.                                                     =");
		System.out.println("= 3. VIEW CART_ITEM.                                           =");
		System.out.println("= 4. SHOPPING_CART.                                            =");
		System.out.println("================================================================");
	}
	
	public static void printMenuBook() {
		System.out.println("================================================================");
		System.out.println("============================ BOOK ==============================");
		System.out.println("= 1. ADD A BOOK.                                               =");
		System.out.println("= 2. UPDATE BOOK INFORMATION.                                  =");
		System.out.println("= 3. SEARCH.                                                   =");
		System.out.println("= 4. DELETE A BOOK.                                            =");
		System.out.println("================================================================");
	}
	
}
